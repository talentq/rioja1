﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Topshelf;
using Autofac;

namespace TestService
{
    public class TownCrier
    {
        readonly Timer _timer;
        private IData DataRepo;
        public TownCrier(IData dataRepo)
        {
            this.DataRepo = dataRepo;
            _timer = new Timer(1000) { AutoReset = true };
            _timer.Elapsed += (sender, eventArgs) => Console.WriteLine("Lottery numbers: {0}", string.Concat(DataRepo.GetData()));
        }
        public void Start() { _timer.Start(); }
        public void Stop() { _timer.Stop(); }
    }

    public class Program
    {
        public static void Main()
        {
            //autofac setup
            var builder = new ContainerBuilder();
            builder.Register<IData>(r => new DataRepo());
            var container = builder.Build();

            string message = "Empty";
            HostFactory.Run(x =>
            {
                x.AddCommandLineDefinition("message", a => message = a);
                x.Service<TownCrier>(s =>
                {
                    s.ConstructUsing(name => new TownCrier(container.Resolve<IData>()));
                    s.WhenStarted(tc => tc.Start());
                    s.WhenStopped(tc => tc.Stop());
                });
                x.RunAsLocalSystem();

                x.SetDescription("Sample Topshelf Host");
                x.SetDisplayName("Stuff");
                x.SetServiceName("Stuff");
            });
        }
    }
}
