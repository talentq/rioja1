﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestService
{
    public interface IData
    {
        IEnumerable<string> GetData();
    }
}
