﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestService
{
    public class DataRepo : IData
    {
        public IEnumerable<string> GetData()
        {
            Random ran = new Random(DateTime.Now.Millisecond);
            for(int i = 0; i < 5; i++)
            {
                yield return ran.Next().ToString();
            }
        }
    }
}
